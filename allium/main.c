//
//  Created by sq3aky on 10/27/21.
//

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "allium.h"



void allium_hash(const char* input, char* output);


int main(int argc, const char * argv[]) {
    
    char const input[80] = "Testing to validate allium hashing function";
    char output[32];
    allium_hash(input, output);
    
    for (int i = 0; i < sizeof output; i ++) {
            printf("%02x", (unsigned char) output[i]);
    }
    putchar('\n');
    return 0;
    
}
